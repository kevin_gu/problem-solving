# problem-solving
```

/*
输入一个不为空的单词列表 返回出现频率最高的前k个的单词。
输出的这K个单词里需要按照频率排序好。

条件1）内存大小不需要考虑
条件2）时间很关键 输入的量有可能很大。

例子1
Input: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
Output: ["i", "love"]
例子2
Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
Output: ["the", "is", "sunny", "day"]

run time?

 */
```

 ```java
//java
import java.util.List;
public class TopKFrequent {
  static String[] inputs = new String[]{"i", "love", "leetcode", "i", "love", "coding"};
  public static List<String> topKFrequent(String[] words, int k) {

    return null;
  }
}
```
